#!/bin/bash

date >> /tmp/ovpn-up.log
env >> /tmp/ovpn-up.log

ip addr show dev "$dev"

if [[ "$script_type" == 'up' ]]
cat >> /tmp/ovpn-cmd.log <<EOF
ip route add $route_vpn_gateway via $route_net_gateway
ifconfig $dev $ifconfig_local $ifconfig_netmask up
ip route add default via $route_vpn_gateway

EOF

