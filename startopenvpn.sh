#/!/bin/bash
#https://api.demo.bitmask.net:4430/1/cert
set -eux
#wget -O combined.pem https://api.demo.bitmask.net:4430/1/cert 
source settings.env

openvpn --setenv LEAPOPENVPN 1 \
--nobind \
--client \
--dev leap \
--dev-type tun \
--tls-client \
--remote-cert-tls server \
--up $OVPNDIR/scripts/up.sh \
--ipchange $OVPNDIR/scripts/up.sh \
--route-up $OVPNDIR/scripts/up.sh \
--down $OVPNDIR/scripts/up.sh \
--script-security 2 \
--user nobody \
--persist-key \
--persist-local-ip \
--group nobody --verb 1 \
--remote 212.83.165.160 443 tcp4 \
--tls-cipher DHE-RSA-AES128-SHA \
--cipher AES-128-CBC \
--auth SHA1 \
--keepalive 10 30 \
--ca $OVPNDIR/ca.pem \
--cert $OVPNDIR/cert.pem \
--key $OVPNDIR/privkey.pem \



