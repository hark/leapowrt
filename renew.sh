#/!/bin/bash
#https://api.demo.bitmask.net:4430/1/cert
set -eux
GITDIR="`git rev-parse --show-toplevel`"

source settings.env
echo $API
echo $OVPNDIR
DIR=`mktemp -d`
cd /tmp
ls -lhtr $DIR
wget --ca-certificate "$OVPNDIR"/"$CA" -O $DIR/combined.pem $API/1/cert 

openssl x509 -subject -noout -in "$DIR"/combined.pem 
openssl x509 -dates -noout -in "$DIR"/combined.pem 
openssl x509 -purpose -noout -in "$DIR"/combined.pem 

openssl rsa -check -inform PEM -pubout -in $DIR/combined.pem > $DIR/pubkey.pem



if openssl x509 -in $DIR/combined.pem  > $DIR/cert.pem
then
    echo "cert success"
    cp $GITDIR/$CA $OVPNDIR/ca.pem
    cp $DIR/cert.pem $OVPNDIR/cert.pem
else
    echo "cert fail"
fi


if openssl rsa -check -inform PEM -in $DIR/combined.pem > $DIR/privkey.pem
then
    echo "privkey succes"
    cp $DIR/privkey.pem $OVPNDIR/privkey.pem

else
    echo "privkey fail"
fi






